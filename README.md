# README #

The codes help detect certain facial features in human beings. This kind of detection is commonly used in emotion/expression recognition algorithms.

### Which Features? ###

* Mouth
* Nose
* Eyes (Both seperately)
* Face

### Running ###

Just clone the repo and run the codes. The XML classifiers are already present in the repository.

### Contribution guidelines ###

* Writing tests
* Code review